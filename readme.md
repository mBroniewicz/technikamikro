# Projekt 1
## mikrokontroler: 328p
#### linijka diód wyświetlająca poziom dźwięku z mikrofonu.

##### Potrzebne elementy:
- Arduino
- Moduł mikrofonu
- 7 Rezystorów 220 Ohm
- 3 diody LED zielone
- 2 diody LED żółte
- 2 diody LED czerwone
- Płytka stykowa

#### Kod programu
```
//void setup() {
  // put your setup code here, to run once:

//}

//void loop() {

//Przypisywanie wartości do pinów wyjściowych
int led1 = 2;
int led2 = 3;
int led3 = 4;
int led4 = 6;
int led5 = 7;
int led6 = 8;
int led7 = 13;
int sensorPin = A0; 
int sensorval = 0; // wartość pobierana z Sound Sensor

// the setup routine runs once when you press reset:
void setup() {
//Piny cyfrowe jako kolejne wyjścia
pinMode(led1, OUTPUT);
pinMode(led2, OUTPUT);
pinMode(led3, OUTPUT);
pinMode(led4, OUTPUT);
pinMode(led5, OUTPUT);
pinMode(led6, OUTPUT);
pinMode(led7, OUTPUT);
Serial.begin(9600); // initialize serial communication with computer
}
// the loop routine runs over and over again forever:
void loop() {
sensorval = analogRead(0); // Analog Read from Sensor
Serial.println(sensorval); // Wypisywanie wartości
digitalWrite(led1, LOW);
digitalWrite(led2, LOW);
digitalWrite(led3, LOW);
digitalWrite(led4, LOW);
digitalWrite(led5, LOW);
digitalWrite(led6, LOW);
digitalWrite(led7, LOW);
//delay(1);
if (sensorval > 525) {digitalWrite(led1, HIGH); }
if (sensorval > 535) {digitalWrite(led2, HIGH); }
if (sensorval > 545) {digitalWrite(led3, HIGH); }
if (sensorval > 555) {digitalWrite(led4, HIGH); }
if (sensorval > 565) {digitalWrite(led5, HIGH); }
if (sensorval > 575) {digitalWrite(led6, HIGH); }
if (sensorval > 575) {digitalWrite(led7, HIGH); }
//delay(10);
}
```

#### Prezentacja działającego projektu:
[![Prezentacja](/IMG_9384.JPG)](https://youtu.be/ojEeupUfh4o)
